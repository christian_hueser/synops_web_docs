# Compile Application 

## Content
  1. Introduction and Overview
  2. Get Source Code from BitBucket
  	1. Setup and Configure Git
	2. Command git clone to Retrieve Source Code
	3. Command git checkout to Select Source Code Version
  3. Setup Eclipse Project
    1. Setup New Project
	2. Compile Sources With Maven
	3. Start Tomcat Server and App
  4. Open App in Browser
  5. Summary
  
## 1 Introduction and Overview

SYNOPS-WEB is an implementation of SYNOPS-GIS for the web.

## 2 Get Source Code from BitBucket

### 2.1 Setup and Configure Git

```bash
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
$ git config --list
```

References:

* [git-scm.com - Book](https://git-scm.com/book/en/v2)
* [git-scm.com - Getting-Started-First-Time-Git-Setup](ttps://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)

### 2.2 Command git clone to Retrieve Source Code

Public respsitories are for free on BitBucket. Register and clone the git repository 
onto your computer within your chosen folder.

```bash
$ git clone https://johndoe@bitbucket.org/todeswiesel/synops.git ./synops/
```

References:

* [BitBucket](https://bitbucket.org)


### 2.3 Command git checkout to Select Source Code Version

The git repository containes several branches, i.e. versions of the app. 
In order to retrieve the version to be installed, you can do the following within the cloned folder:

```bash
$ git checkout master
```

## 3 Setup Eclipse Project

### 3.1 Setup New Project

### 3.2 Compile Sources With Maven

### 3.3 Start Tomcat Server and App

## 4 Open App in Browser

## 5 Summary
